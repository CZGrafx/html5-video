window.addEventListener('load', function() {
    // VIDEO CONTAINER
    video = document.getElementById('video');
    pauseScreen = document.getElementById('screen');
    screenButton = document.getElementById('screenButton')

    // PROGRESS BAR CONTAINER
    pbarContainer = document.getElementById('pbar-container');
    pbar = document.getElementById('pbar');


    // BUTTONS CONTAINER
    playButton = document.getElementById('play-button');
    timeField = document.getElementById('time-field');
    soundButton = document.getElementById('sound-button');
    sbarContainer = document.getElementById('sbar-container');
    sbar = document.getElementById('sbar');
    fullscreenButton = document.getElementById('fullscreen-button')
    
    
    video.load();
    video.addEventListener('canplay', function() {
            playButton.addEventListener('click', playOrPause, false);
            pbarContainer.addEventListener('click', skip, false);
            updatePlayer();
            soundButton.addEventListener('click', muteOrUnmute, false);
            sbarContainer.addEventListener('click', changeVolume, false);
            fullscreenButton.addEventListener('click', fullscreen, false)
            
            screenButton.addEventListener('click', playOrPause, false);
            
        }, false);
    }, false);
    
    
    function playOrPause() {
    	if(video.paused) {
    	    video.play();
    	    document.getElementById("play-button").className = "fa fa-pause fa-3x";
    	    updateProgress = setInterval(updatePlayer, 30);
    	    
    	    pauseScreen.style.display = 'none';
//     	    document.getElementById("play-button").className = "fa fa-play fa-3x";
    	} else {
    	    video.pause();
    	    document.getElementById("play-button").className = "fa fa-play fa-3x";
    	    window.clearInterval(updateProgress);
    	    
    	    pauseScreen.style.display = 'block';
    	    document.getElementById("play-button").className = "fa fa-play fa-3x";
    	}
    }
    
    function skip(ev) {
       var mouseX = ev.target.getBoundingClientRect();
       var px = ev.clientX - mouseX.left;
       var barWidth = window.getComputedStyle(pbarContainer).getPropertyValue('width');
       barWidth = parseFloat(barWidth.substr(0, barWidth.length - 2));
       
       video.currentTime = (px/barWidth)*video.duration;
       updatePlayer();
    }
    function updatePlayer() {
        var percentage = (video.currentTime/video.duration)*100;
        pbar.style.width = percentage + '%';
        
        timeField.innerHTML = getFormattedTime();
        
        if(video.ended) {
            window.clearInterval(updateProgress);
    	    document.getElementById("play-button").className = "fa fa-repeat fa-3x";
    	    
    	    pauseScreen.style.display = 'block';
    	    document.getElementById("screenButton").className = "fa fa-repeat fa-3x";    	    
        } else if(video.pause) {
    	    document.getElementById("play-button").className = "fa fa-play fa-3x";            
    	    document.getElementById("screenButton").className = "fa fa-play fa-3x";    	    
        }
    }
    
    function getFormattedTime() {
        var seconds = Math.round(video.currentTime);
        var minutes = Math.floor(seconds/60);
        if(minutes > 0) seconds -= minutes*60;
        if(seconds.toString().length === 1) seconds = '0' + seconds;
            
        var totalSeconds = Math.round(video.duration);
        var totalMinutes = Math.floor(totalSeconds/60);
        if(totalMinutes > 0) totalSeconds -= totalMinutes*60;
        if(totalSeconds.toString().length === 1) totalSeconds = '0' + totalSeconds;
                 
        return minutes + ':' + seconds + ' / ' + totalMinutes + ':' + totalSeconds;
    }
        
    function muteOrUnmute() {
        if(!video.muted) {
            video.muted = true;
    	    document.getElementById("sound-button").className = "fa fa-volume-off fa-3x";
            sbar.style.display = 'none';
              
        } else {
            video.muted = false;
    	    document.getElementById("sound-button").className = "fa fa-volume-up fa-3x";
            sbar.style.display = 'block';
        }
    }
    
    function changeVolume(ev) {
        var smouseX = ev.target.getBoundingClientRect();
        var spx = ev.clientX - smouseX.left;
        var sbarWidth = window.getComputedStyle(sbarContainer).getPropertyValue('width');
        sbarWidth = parseFloat(sbarWidth.substr(0, sbarWidth.length - 2));
        
        video.volume = (spx/sbarWidth);
        sbar.style.width = (spx/sbarWidth)*100 + '%';
        video.muted = false;
        document.getElementById("sound-button").className = "fa fa-volume-up fa-3x";
        sbar.style.display = 'block';

   }
   
    function fullscreen() {
        if(video.requestFullscreen) {
            video.requestFullscreen();
        } else if(video.webkitRequestFullscreen) {
            video.webkitRequestFullscreen();        
        } else if(video.mozRequestFullScreen) {
            video.mozRequestFullScreen();        
        } else if(video.msRequestFullscreen) {
            video.msRequestFullscreen();        
        }
    }